# LTU Engine - Python Client

## ABOUT THIS CLIENT
LTUTech is a company that provides image recognition as a service.
This Python module is a client that allows you to access the HTTP API to
perform image recognition tasks. For more information on image recognition,
please visit http://www.ltutech.com.


Note that this client cannot be used without a valid application key. Please
contact our support department to get your application key today
(support@ltutech.com).

## INSTALL
The client comes in the form of a multiplatform python package. Although the
package has been tested on linux platforms only, it should run fine under
Windows.

Note that python 2.6.+ or later is required.
To know if python is already installed or to check the version use the following command:
```bash
python --version
```

First, install dependencies (virtualenv and libs) by typing the following command in a terminal:
```bash
sudo apt-get install virtualenv libpq-dev
```

Then, create and activate your dev environment in the project folder.
```bash
cd ltuengine-python-client
virtualenv env
source env/bin/activate
```

The package can be installed along with dependencies by running:
```bash
python setup.py install
```

## BASIC USAGE
Adding images to the application is done through the add_image() function of a ModifyClient instance:
```python
my_application_key = "replace by your own key"
from ltu.engine.client import ModifyClient
modify_client = ModifyClient(my_application_key)
print(modify_client.add_image("/home/user/image.jpg", "my_image_id"))
```

Once you have at least one image in your application, you can start making
search queries by using the search_image() function of a  QueryClient instance:
```python
from ltu.engine.client import QueryClient
query_client = QueryClient(my_application_key)
print(query_client.search_image("/home/user/image.jpg"))
```

## ADVANCED USAGE

For advanced usage, please consult the docstrings for each function.

## EXEMPLE
An example is provided to quickly test the ADD, SEARCH and DELETE API functions.
You have to execute the file cli.py by specifing these parameters:
Positional arguments:
  ACTION: add, search, delete or bench(test the 3 functions in one action)
  APPLICATION_KEY
  INPUT_DIR

Optional arguments:
  --help                show help message and exit
  --host HOST, -h HOST  (default:  None)
  --nb-threads NB_THREADS, -n NB_THREADS
                        (default: 1)
  --force               force actions already processed on a file

Result of queries are saved in a Json file in the folder "out_result". Every actions already performed on a file won't be executed a second time (except in bench mode) unless the parameter --force is specified.

Here, an example for the ADD feature. A folder containing 5 images is available in this client: ./data
```bash
python ./ltu/engine/cli.py add application_key --force ./data -n 4
```

If the parameter host is not specified, the script will hit the LTU OnDemand API.
Otherwise, you can specify your own server by adding the domain name or the IP address after --host.
Don't forget to specify http or https.
```bash
python ./ltu/engine/cli.py add application_key ./data --host http://10.5.20.56 -n 4
python ./ltu/engine/cli.py add application_key ./data --host http://mydomainname.com -n 4
```
### Comment
With python3 environment, cli_v3.py can be used instead of cli.py for better help.

## ADMIN
admin.py (respectively admin_v3.py) download image and their thumbnails or all images and their thumbails for given application.
You have to execute the file admin.py by specifing these parameters:

positional arguments:
  ACTIONS               A list of actions to execute on an application:
                        download image and this thumbnail or download all the
                        content : dl_img, dl_all_img, dl_img_thumbnail,
                        dl_all_img_thumbnail
  APPLICATION_KEY       LTU Engine application private key
  DB_PWD                LTU Engine application database password

optional arguments:
  --help                show this help message and exit
  --image-id IMAGE_ID, -i IMAGE_ID
                        image name (default: None)
  --out-path OUT_PATH, -o OUT_PATH
                        folder path where save image to dowload (default:
                        ./out/)
  --size SIZE, -s SIZE  thumbnail height (default: 0)
  --host HOST, -h HOST  server URL that hosts the application, default is LTU
                        OnDemand (default: None)
  --nb-threads NB_THREADS, -n NB_THREADS
                        a list(separate each action by a comma) of number of
                        threads (default: 1)

### EXEMPLES
- download image:
```bash
  ./ltu/engine/admin_v3.py dl_img APPLICATION_KEY DB_PWD --host http://mydomainname.com --out-path /tmp/ltu_download --image-id IMAGE_ID
```

- download image and all associates thumbnails:
```bash
  ./ltu/engine/admin_v3.py dl_img_thumbnail APPLICATION_KEY DB_PWD --host http://mydomainname.com --out-path /tmp/ltu_download --image-id IMAGE_ID
```

- download image and (64,64) associate thumbnail:
 ```bash
  ./ltu/engine/admin_v3.py dl_img_thumbnail APPLICATION_KEY DB_PWD --host http://mydomainname.com --out-path /tmp/ltu_download --image-id IMAGE_ID -s 64
```

- download all images:
```bash
 ./ltu/engine/admin_v3.py dl_all_img APPLICATION_KEY DB_PWD --host http://mydomainname.com --out-path /tmp/ltu_download
```

- download all images and all associates thumbnails:
```bash
  ./ltu/engine/admin_v3.py dl_all_img_thumbnail APPLICATION_KEY DB_PWD --host http://mydomainname.com --out-path /tmp/ltu_download
```

- download all images and (200, 200) associates thumbnails:
```bash
  ./ltu/engine/admin_v3.py dl_all_img_thumbnail APPLICATION_KEY DB_PWD --host http://mydomainname.com --out-path /tmp/ltu_download -s 200
```

## LICENSE
This software is licensed under the terms of the Apache License 2.0. In
particular, you are free to distribute it, modify it and to distribute modified
versions as long as you include the attached NOTICE file with your software.
Read the attached LICENSE file for more information.

## CREDITS & USAGE:
Feel free to use any of the resources provided in this client.

EMAIL: support@ltutech.com
