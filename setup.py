#!/usr/bin/env python
from setuptools import find_packages, setup

with open('requirement/requires.txt') as f:
    INSTALL_DEPENDENCIES = f.read().splitlines()

SETUP_DEPENDENCIES = ['setuptools_scm==5.0.1']

setup(name='ltuengine-python-client',
      version='1.2',
      author=u'LTUtech',
      author_email='support@ltutech.com',
      url='unknown',
      description='LTU Engine API Python client.',
      license='LICENSE',
      scripts=[],
      use_scm_version=True,
      include_package_data=True,
      packages=find_packages(),
      setup_requires=SETUP_DEPENDENCIES,
      install_requires=INSTALL_DEPENDENCIES
      )
