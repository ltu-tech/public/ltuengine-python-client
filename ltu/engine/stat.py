import logging
import threading

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class Stat(object):
    """Manage the queries and images statistics
    """

    def __init__(self):
        self.dict = {}

        # mutex
        self.mutex = threading.Lock()

    def incremente(self, path, step=1):
        """incremente of value step a counter for each key contained in path
        """
        self.mutex.acquire()
        keys = path.split('.')
        current_element = self.dict
        for key in keys:
            if '_count' not in current_element:
                # we create a new counter
               current_element['_count'] = step
            else:
                # we increment existing key/value
               current_element['_count'] = current_element['_count'] + step
            if key not in current_element:
               current_element[key] = {'_count': 0}
               # check last key
            if key == keys[-1]:
               # we reach the key of the path
               # we increment existing key/value
               current_element[key]['_count'] = current_element[key]['_count'] + step
            else:
               # we reach the next key of the path
               current_element = current_element[key]

        logger.debug(self.dict)
        self.mutex.release()

    def add_and_average_stat(self, path, value, average=False):
        """ add a value to a statistic. The second time we add this value, we make an average """
        self.mutex.acquire()
        keys = path.split('.')
        current_element = self.dict
        for key in keys:
            # check last key
            if key == keys[-1]:
                 # we reach the key of the path
                 # we add  key/value
                if key in current_element:
                    current_element[key]['value'] += float(value)
                    current_element[key]['_count'] += 1
                    current_element[key]['average_count'] += 1
                    if average:
                        current_value = float(current_element[key]['value'])
                        nb_cmpt = float(current_element[key]['average_count'])
                        current_element[key]['value'] = current_value/nb_cmpt
                        current_element[key]['average_count'] = 1
                else:
                    current_element[key] = {'_count': 1, 'value': value, 'average_count': 1}
            else:
                 if key not in current_element:
                    # create a new dict
                    current_element[key] = {'_count': 1}
                 else:
                    current_element[key]['_count'] += 1
                 # we reach the next key of the path
                 current_element = current_element[key]
        logger.debug(self.dict)
        self.mutex.release()

    def get_count(self, path):
        """ retour the count value of a stat"""
        keys = path.split('.')
        current_element = self.dict
        for key in keys:
            if key in current_element:
                if key == keys[-1]:
                    return current_element[key]['_count']
                else:
                    current_element = current_element[key]
            else:
                return 0
        return 0

    def get_element(self, path):
        """ retour the value af a stat"""
        keys = path.split('.')
        current_element = self.dict
        for key in keys:
            if key in current_element:
                if key == keys[-1]:
                    if key in current_element:
                        return current_element[key]
                    return 0
                else:
                    current_element = current_element[key]
            else:
                return 0
        return 0

    def get_total(self, key, path=None, data=None, count=0):
        """ return the sum of all the count values for key from a path
            if no path is specified, the count is totalized in all the dict
        """
        if data is None:
            data = self.dict
        if path:
            # starting path
            keys = path.split('.')
            current_element = data
            for k in keys:
                if k in current_element:
                    data = current_element[k]
                else:
                    continue

        for k, value in data.items():
            if type(value) != dict:
                continue
            if "_count" not in value:
                continue
            if k == key:
                count = count + value['_count']
            if k == '_count':
                continue
            count = self.get_total(key, None, value, count)
        return count

    def print_stats(self, data=None, indent=0):
       """ print all the values of the dict"""
       if data is None:
           data = self.dict
       for key, value in data.items():
           if key == '_count':
               continue
           if type(value) == dict:
               print("{} - {}: {}".format(" " * indent, key, value['_count']))
               self.print_stats(value, indent+2)
           else:
               print("{} - {}: {}".format(" " * indent, key, value))
