import json
import logging
import os
import requests
import hashlib
from requests.packages.urllib3.util.retry import Retry
from requests.adapters import HTTPAdapter

from ltu.engine.result import Result, FICResult

# disable some requests warning and info messages
requests.packages.urllib3.disable_warnings()
logger = logging.getLogger('__main__')
logging.getLogger('requests.packages.urllib3.connectionpool').setLevel(logging.ERROR)


class BaseClient(object):
  """Base class from which ModifyClient and QueryClient inherit.

  This class contains basic methods for accessing the API.
  """

  def __init__(self, application_key, server_url, app_public_key=None):
    """Constructor
    Args:
      application_key:  authentication key provided by the application.
      server_url:       complete http url to the OnDemand server.
    """
    self.application_key = application_key
    self.app_public_key = app_public_key
    self.base_server_url = server_url.rstrip('/')
    self.server_url = self.base_server_url

    # connection rebustness with retries ( see:BaseClient._connet )
    self.session = requests.Session()
    retries = Retry(total=1,
                    backoff_factor=0.1,
                    status_forcelist=[500, 502, 503, 504])
    self.session.mount('http://', HTTPAdapter(max_retries=retries,
                       pool_connections=5, pool_maxsize=200))
    self.session.mount('https://', HTTPAdapter(max_retries=retries,
                       pool_connections=5, pool_maxsize=200))

  def _connect(self, url_list=[]):
    """Connect to remote server using a list of possible API paths.
       It will try them all to perform auto-discovery of the right URL.
    """
    logger.info('Auto discovering API path with base server URL: %s' % self.base_server_url)
    full_urls = [self.base_server_url] + ["%s%s" % (self.base_server_url, url) for url in url_list]
    # Test all API paths
    for url in full_urls:
      try:
        self.server_url = url
        logger.info('Testing URL: %s' % self.server_url)
        status = self.check_status()
        if status:
          logger.info('API path found at: %s' % url)
          return True
      except:
        pass
    # improve connection rebustness with retries
    retries = Retry(total=5,
                    backoff_factor=0.1,
                    status_forcelist=[500, 502, 503, 504])
    self.session.mount('http://', HTTPAdapter(max_retries=retries))
    self.session.mount('https://', HTTPAdapter(max_retries=retries))

    raise Exception("Could not connect to your application")

  def get_url(self, service):
    """Combine a service name and the server url to produce the service url.
    """
    logger.debug("Joining URLs %s and %s" % (self.server_url, service))
    url = requests.compat.urljoin("%s/" % self.server_url, service)
    logger.debug("Final URL: %s" % url)
    return url

  def get_data(self, params={}):
    """Return appropriate HTTP POST parameters and optional file

    The application key is automatically added.

    Args:
      params: a dictionary with service-specific parameters
    Returns:
      filtered_params, files to be passed to requests.
    """
    data = [("application_key", self.application_key)]
    for key, val in params.items():
      if val is not None:
        if isinstance(val, (list, tuple, set)):
          for v in val:
            data.append((key, v))
        else:
          data.append((key, val))

    return data

  def check_status(self):
    """Check that this client can successfully access your application.

    Logs advice on actions to take as warnings in case of wrong status.
    """
    try:
      result = self.get_application_status()
      if result.status_code == 0:
        return True
      else:
        return False
    except:
      return False

  def open_service(self, service, params={}, files=None):
    """Open corresponding API service with appropriate parameters.

    Args:
      service: service name, e.g: GetApplicationStatus
      params: a dictionary of arguments to be passed to the service
    Returns:
      The response content.
    """
    data = self.get_data(params)
    url = self.get_url(service)
    logger.debug("Sending '%s' request to: %s" % (service, url))
    response = self.session.post(url, data=data, files=files, verify=False)
    # check that we do not have HTTP error first
    response.raise_for_status()
    return response.text

  def get_application_status(self):
    """Check the application status.

    Example:
      result = client.get_application_status()
      if(result.status_code < 0):
        raise Exception(result.status_message)
    """
    result = self.open_service("GetApplicationStatus")
    return Result(result)

  def get_image(self, image_id):
    """Search for an image based on its id."""
    result = self.open_service("GetImageById", params={"image_id": image_id})
    return Result(result)


class QueryClient(BaseClient):
  """Client that can run searches on an LTU Engine application."""

  DEFAULT_QUERY_URL = "https://api.ltu-engine.com"

  def __init__(self, application_key, server_url=None, app_public_key=None):
    """Constructor

    Args:
      application_key: authentication key provided by the application.
      server_url: complete http url to the OnDemand server. If it not
                  specified, it will default to the default url.
    """
    logger.info('Starting query client...')
    self.server_url = server_url
    if not server_url:
      self.server_url = QueryClient.DEFAULT_QUERY_URL
    BaseClient.__init__(self, application_key, self.server_url, app_public_key)
    url_list = [':8080/api/v2.0/ltuquery/json/',
                '/api/v2.0/ltuquery/json/',
                ':8080/api/v2/ltuquery/json/',
                ':8080/v2/ltuquery/json/',
                '/api/v2/ltuquery/json/',
                '/v2/ltuquery/json/',
                ]
    BaseClient._connect(self, url_list)

  def search_image(self, image, params={}):
    """Image retrieval based on a image stored on disk

    Args:
      image: path to image file.
    """
    with open(image, 'rb') as img:
      result = self.open_service("SearchImageByUpload",
                                 files={"image_content": img},
                                 params=params)
    return Result(result)

  def search_image_buffer(self, image_body, params={}):
    """Image retrieval based on a image buffer

    Args:
      image_body: image content.
    """
    result = self.open_service("SearchImageByUpload",
                               files={"image_content": image_body},
                               params=params)
    return Result(result)

  def search_image_by_id(self, image_id, params={}):
    """Image retrieval based on a image ID already present in the application.

    Args:
      image_id: ...
      NOTE: search_image_by_id might not work on matching application of familly 72.
    """
    params["image_id"] = image_id
    result = self.open_service("SearchImageById",
                               params=params)
    return Result(result)

  # TODO test this
  def search_image_by_keywords(self, keywords, starting_index=None,
                               nb_results=None, ids_list=None):
    """Search all images with associated keywords.

    Args:
      keywords: an iterator on a keyword strings
    """
    result = self.open_service("SearchImageByKeywords",
                               params={"keywords": keywords, "starting_index":
                                       starting_index, "nb_results":
                                       nb_results, "ids_list": ids_list})
    return Result(result)

  def fine_image_comparison(self, reference_image, query_image):
    """Compare two images using the Fine Image Comparison.
    NOTE: This features works only on application with Fine
    Image Comparison enabled. Contact the support for more
    information.

    Args:
      reference_image: path to reference image file.
      query_image: path to query image file.
    """
    try:
        files = {}
        files['reference_image'] = open(reference_image, 'rb')
        files['query_image'] = open(query_image, 'rb')
    except Exception as e:
        raise Exception('Could not open one of the input files: {}'.format(e))

    result = self.open_service("FineComparison", files=files)
    return FICResult(result)

  def get_image_url(self, image_id):
    image_name = image_id.encode("utf")
    image_md5 = hashlib.md5(image_name)
    md5_value = image_md5.hexdigest()

    if "ltu-engine" in self.server_url:
        img_url = "https://static.ltu-engine.com/image/%s/%s" % (
          self.app_public_key, md5_value)
    else:
        img_url = self.base_server_url
        img_url += ":8888/image/%s/%s" % (self.app_public_key, md5_value)
    return img_url

  def get_thumbnail_image_url(self, image_id, w, l):
    width = str(w)
    lengh = str(l)
    image_name = image_id.encode("utf")
    image_md5 = hashlib.md5(image_name)
    md5_value = image_md5.hexdigest()

    if "ltu-engine" in self.server_url:
        img_url = "https://static.ltu-engine.com/thumb-%sx%s/image/%s/%s" % (
          width, lengh, self.app_public_key, md5_value)
    else:
        img_url = self.base_server_url
        img_url += ":8888/image/thumb-%sx%s/%s/%s" % (width, lengh, self.app_public_key, md5_value)

    return img_url

  def download_image(self, image_url, img_out_path):
    response = requests.get(image_url)
    if response.status_code == 200:
      with open(img_out_path, 'wb') as f:
        logger.debug("download %s in the directory %s" % (image_url, img_out_path))
        f.write(response.content)


class ModifyClient(BaseClient):
  """Client that can modify an LTU Engine application, e.g: by adding and
  removing images."""

  DEFAULT_MODIFY_URL = "https://api.ltu-engine.com"

  def __init__(self, application_key, server_url=None, app_public_key=None):
    """Constructor
    Args:
      application_key:  authentication key provided by the application.
      server_url:       complete http url to the OnDemand server. If it is not
                        specified, it will default to the default url.
    """
    logger.info('Starting modify client...')
    if not server_url:
      server_url = ModifyClient.DEFAULT_MODIFY_URL
    BaseClient.__init__(self, application_key, server_url, app_public_key)
    url_list = [':7789/api/v2.0/ltumodify/json/',
                '/api/v2.0/ltumodify/json/',
                ':7789/api/v2/ltumodify/json/',
                ':7789/v2/ltumodify/json/',
                '/api/v2/ltumodify/json/',
                '/v2/ltumodify/json/',
                ]
    BaseClient._connect(self, url_list)

  def add_image(self, image, image_id=None, keywords=[], check_exist_first=True):
    """Add an image to the database

    Args:
      image:    path to image file
      image_id: any unique identifier
      keywords: an iterator on a keyword strings
    """
    if not image_id:
      image_id = os.path.basename(image)

    # get keyword of image in Image_Name.keyword
    keyword_file_path = image + '.keyword'
    if os.path.exists(keyword_file_path):
      with open(keyword_file_path, 'r') as keyfile:
        keywords = keyfile.readlines()

      keywords = [keyword.replace('\n', '') for keyword in keywords]
      logger.debug("keywords associated with the image %s is %s" % (image, keywords))

    # check if image is already in the app
    if check_exist_first and self.get_image(image_id).status_code == 0:
      # TODO: check keywords are equal, if not update them?
      logger.debug("Image {} already in the database".format(image_id))
      result = {'code': 0}
      return Result(json.dumps(result))

    # Add the image
    with open(image, 'rb') as img:
      result = self.open_service("AddImage",
                                 params={"image_id": image_id, "keywords": keywords},
                                 files={"image_content": img})
    return Result(result)

  def delete_image(self, image_id):
    """Remove an image from the database"""
    result = self.open_service("DeleteImage", params={"image_id": image_id})
    return Result(result)

  def delete_imagefile(self, file):
    image_id = os.path.basename(file)
    return self.delete_image(image_id)
