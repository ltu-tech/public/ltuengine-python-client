#! /usr/bin/env python
import sys
import os
import unittest
from ltu.engine.stat import Stat


class TestStat(unittest.TestCase):
    def setUp(self):
        self.longMessage = True
        self.stat = Stat()
        self.stat.incremente("pixou_familly." + "pixou")
        self.stat.incremente("pixou_familly.donald")
        self.stat.incremente("pixou_familly.nephews.fifi")
        self.stat.incremente("pixou_familly.nephews.riri")
        self.stat.incremente("pixou_familly.nephews.loulou")

        self.stat.add_and_average_stat("accountancy.sum", 17)
        self.stat.add_and_average_stat("calculator.sum", 6)
        self.stat.add_and_average_stat("calculator.buffer.sum", 81)

    def test_get_count(self):
        self.assertEqual(self.stat.get_count("pixou_familly"), 5)
        self.assertEqual(self.stat.get_count("pixou_familly.nephews"), 3)
        self.assertEqual(self.stat.get_count("pixou_familly.nephews.riri"), 1)

        self.stat.incremente("pixou_familly.donald")
        self.stat.incremente("pixou_familly.nephews.riri")

        self.assertEqual(self.stat.get_count("pixou_familly"), 7)
        self.assertEqual(self.stat.get_count("pixou_familly.nephews"), 4)
        self.assertEqual(self.stat.get_count("pixou_familly.nephews.riri"), 2)

    def test_get_element(self):
        # get last node
        self.assertEqual(self.stat.get_element("accountancy.sum.average_count"), 1)
        self.assertEqual(self.stat.get_element("accountancy.sum.value"), 17)
        self.assertEqual(self.stat.get_count("accountancy.sum"), 1)

        self.stat.add_and_average_stat("accountancy.sum", 20)
        self.stat.add_and_average_stat("accountancy.sum", 30)
        self.stat.add_and_average_stat("accountancy.sum", 50)

        self.assertEqual(self.stat.get_element("accountancy.sum.average_count"), 4)
        self.assertEqual(self.stat.get_element("accountancy.sum.value"), 117)
        self.assertEqual(self.stat.get_count("accountancy.sum"), 4)

        # average
        self.stat.add_and_average_stat("accountancy.sum", 8, True)

        self.assertEqual(self.stat.get_element("accountancy.sum.average_count"), 1)
        self.assertEqual(self.stat.get_element("accountancy.sum.value"), 25)
        self.assertEqual(self.stat.get_count("accountancy.sum"), 5)

        # get branch of stat tree
        nephews_branch = {'fifi': {'_count': 1}, 'riri': {
            '_count': 1}, '_count': 3, 'loulou': {'_count': 1}}
        self.assertEqual(self.stat.get_element("pixou_familly.nephews"), nephews_branch)

        sum_branch = {'average_count': 1, '_count': 5, 'value': 25}
        self.assertEqual(self.stat.get_element("accountancy.sum"), sum_branch)

    def test_get_total(self):

        self.assertEqual(self.stat.get_total("nephews"), 3)
        self.assertEqual(self.stat.get_total("sum"), 3)
        self.assertEqual(self.stat.get_total("unknown"), 0)

        calculator_branch = self.stat.get_element("calculator")

        self.assertEqual(self.stat.get_total("sum", data=calculator_branch), 2)
        self.assertEqual(self.stat.get_total("sum", "calculator"), 2)

        self.stat.add_and_average_stat("calculator.buffer.sum", 9)
        self.stat.add_and_average_stat("calculator.buffer.sum", 10)

        self.assertEqual(self.stat.get_element("calculator.buffer.sum.value"), 100)
        self.assertEqual(self.stat.get_total("sum", "buffer", calculator_branch), 3)
        self.assertEqual(self.stat.get_total("sum", "buffer", calculator_branch, 13), 16)

    def test_print_stats(self):
        # to assure test_print_stats is interpreted by python
        sys.stdout = open(os.devnull, 'w')
        try:
            self.stat.print_stats()
            self.stat.print_stats(self.stat.get_element("pixou_familly.nephews"), indent=10)
        except Exception as e:
            self.fail("Print stat failed: %s" % e)
        sys.stdout = sys.__stdout__
