import hashlib
import logging
import psycopg2
import os
import socket

from client import QueryClient
from ltu.engine.utils import mkdir_recursive

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class Application(object):
    def __init__(self, host, application_key, dbsi_host, dbapp_host, db_user, db_pwd, db_port):
        self.host = host
        self.dbsi_host_ip = self.getHostIP(dbsi_host)
        if dbapp_host:
            self.dbapp_host_ip = self.getHostIP(dbapp_host)
        else:
            # it no dbapp_host have been provided, we use dbsi_host
            self.dbapp_host_ip = self.dbsi_host_ip
        self.application_key = application_key
        self.db_pwd = db_pwd
        self.db_user = db_user
        self.port = db_port

        self.app_id = self.findApplicationId()
        self.app_public_key = self.findApplicationPublicKey()

        self.query_client = QueryClient(self.application_key, host, self.app_public_key)

    def getHostIP(self, host):
        if host:
            host_fqdn = host.replace("https://", "").replace("http://", "")
            host_fqdn = host_fqdn.split("/")[0].split(":")[0]
            logger.info("Engine host FQDN: %s" % host_fqdn)
            ip = socket.gethostbyname(host_fqdn)  # TODO: better management for SAS mode
            return ip
        else:
            return None

    def databaseSiConnect(self):
        try:
            logger.info("Connecting to DB: {} on {}".format('saas_si', self.dbsi_host_ip))
            conn = psycopg2.connect(dbname='saas_si', user=self.db_user,
                                    hostaddr=self.dbsi_host_ip, port=self.port,
                                    password=self.db_pwd)
            logger.info("Connected to DB: {} on {}".format('saas_si', self.dbsi_host_ip))
        except Exception as e:
            raise IOError("unable to connect to the database. Check your settings. %s" % e)
        return conn

    def databaseAPPConnect(self, db_name, host_ip, user, password):
        try:
            logger.info("Connecting to DB: {} on {}".format(db_name, host_ip))
            conn = psycopg2.connect(dbname=db_name, user=user,
                                    hostaddr=host_ip, port=self.port, password=password)
            logger.info("Connected to DB: {} on {}".format(db_name, host_ip))
        except Exception as e:
            raise IOError("unable to connect to the database. Check your settings. %s" % e)
        return conn

    def dbSqlRequest(self, db_connection, sql_request):
        try:
            cur = db_connection.cursor()
            cur.execute(sql_request)
            sql_result = cur.fetchall()
        except Exception as e:
            raise IOError("Error occured when reading the database. Check application key. %s" % e)

        return sql_result

    def findApplicationId(self):
        """
        Ask database to find id of the given application_key
        """
        try:
            saas_connection = self.databaseSiConnect()
            sql_request = "select id_application from  application where application_key = '{}'"\
                          .format(self.application_key)
            sql_result = self.dbSqlRequest(saas_connection, sql_request)
            app_id = sql_result[0][0]
            logger.info("Application {} id is: {}".format(self.application_key, app_id))
        except Exception as e:
            raise IOError("Error occured when reading the database. Check your settings. %s" % e)
        return app_id

    def findDBAppLogin(self):
        """
        Ask database to find id of the given database password
        """
        saas_connection = self.databaseSiConnect()
        sql_request = "select value from application_property "\
                      "where id_application={} "\
                      "AND name='database_password'" \
                      .format(self.app_id)
        sql_result = self.dbSqlRequest(saas_connection, sql_request)
        password = sql_result[0][0]

        return password

    def findApplicationPublicKey(self):
        """
        Ask database to find id of the given application_key
        """
        saas_connection = self.databaseSiConnect()
        sql_request = "select folder_path from application, client "\
                      "where application.id_client = client.id_client "\
                      "AND application.id_application = {}"\
                      .format(self.app_id)
        sql_result = self.dbSqlRequest(saas_connection, sql_request)
        part_on = sql_result[0][0]

        sql_request = "select value from application_property "\
                      "where id_application = {} AND name = 'folder_path'"\
                      .format(self.app_id)
        sql_result = self.dbSqlRequest(saas_connection, sql_request)
        part_two = sql_result[0][0]

        concat = part_on + part_two
        app_public_key = concat.replace("/", "")

        return app_public_key

    def findApplicationImage(self):
        """
        ask database with application_id to find and return image
        return a list of tuple (image_id, image name)
        """
        app_id = self.findApplicationId()
        db_name = "saas_app_{}".format(app_id)
        db_password = self.findDBAppLogin()
        db_app_connection = self.databaseAPPConnect(db_name, self.dbapp_host_ip,
                                                    db_name, db_password)
        sql_request = "select ltu_signature_id, ltu_kimage_id from image;"
        sql_result = self.dbSqlRequest(db_app_connection, sql_request)

        return sql_result

    def findApplicationImageKeywords(self):
        """
        ask database with application_id to find and return image
        return a list of tuple (image_id, image name)
        """
        app_id = self.findApplicationId()
        db_name = "saas_app_{}".format(app_id)
        db_password = self.findDBAppLogin()
        db_app_connection = self.databaseAPPConnect(db_name, self.dbapp_host_ip,
                                                    db_name, db_password)
        sql_request = "select ltu_kimage_id, array_agg(keyword) "\
                      "from keyword group by ltu_kimage_id;"
        sql_result = self.dbSqlRequest(db_app_connection, sql_request)
        ldKeywords = {}
        for id, keywords in sql_result:
            ldKeywords["%s" % id] = keywords
        return ldKeywords

    def findImageUrl(self, image_id, size=0):
        """
        determine image url
        """
        if size == 0:
            img_url = self.query_client.get_image_url(image_id)
        else:
            height, width = size
            img_url = self.query_client.get_thumbnail_image_url(image_id, height, width)
            logger.debug(
                "The thumbnail of the image %s is available at this adress: %s", image_id, img_url)

        return img_url

    def getImagePath(self, image_id, out_path, size=0):
        """
        determine image outpath if downloaded
        """
        # compute md5
        image_name = image_id.encode("utf")
        image_md5 = hashlib.md5(image_name)
        image_md5_value = image_md5.hexdigest()

        # folder management
        md5_directory = os.path.join(
            image_md5_value[0:2], image_md5_value[2:4], image_md5_value[4:6])

        if size == 0:
            img_out_path = os.path.join(out_path, md5_directory, image_id)
        else:
            out_path = os.path.join(out_path, str(size), md5_directory)
            height, width = size
            img_out_path = out_path + "/" + "thumb-" + \
                str(height) + "x" + str(width) + "_" + image_id

        return img_out_path

    def downloadImage(self, image_id, out_path, size=0, keywords=None):
        """
        download image in out_path folder
        """
        logger.debug("Downloading image: {}".format(image_id))
        # compute download path and create folder
        img_out_path = self.getImagePath(image_id, out_path, size)
        mkdir_recursive(os.path.dirname(img_out_path))

        # find image url and download it if not on the disk
        if not os.path.isfile(img_out_path):
            img_url = self.findImageUrl(image_id, size)
            self.query_client.download_image(img_url, img_out_path)
            # if we have keywords we save them in *.keyword file
            if keywords:
                with open("{}.keyword".format(img_out_path), "wt") as fKeywords:
                    # one keyword per line
                    fKeywords.write("\n".join(keywords))

    def downloadThumbnailImage(self, image_id, out_path, dl_thumbnail, size, keywords):
        """
        download image in out_path folder and associate thumbnail if bool dl_thumbnail set to true
        """
        self.downloadImage(image_id, out_path, keywords=keywords)

        if dl_thumbnail:
            # download image thumbnails
            if size != 0:
                thumbnail_size = [size]
            else:
                # all thumbnails
                thumbnail_size = [(32, 32), (64, 64), (128, 128),
                                   (150, 100), (200, 200), (512, 512)]

            for resolution in thumbnail_size:
                logger.debug("Downloading thumbnails: {}%{}resolution".format(image_id, resolution))
                self.downloadImage(image_id, out_path, resolution, keywords)

    def downloadThumbnailImageForThread(self, item):
        """
        item = [image_id, out_path, dl_thumbnail, size]
        """
        self.downloadThumbnailImage(*item)
