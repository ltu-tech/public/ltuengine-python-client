import threading
import os
import logging
import sys

from datetime import datetime

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class LTUErrors(object):
    """ Manage the LTU errors
        Get the meaning according an erros code
        save in a log file when it is specified
    """

    def __init__(self, bLog=False):

        self.logToFile = bLog
        self.errorsDict = {}
        self.errorsTypeFile = os.path.dirname(os.path.abspath(__file__)) + '/ltu_errors_types.txt'

        try:
            with open(self.errorsTypeFile) as f:
                self.errorsDict = {code: error for code,
                                   error in (l.rstrip().split('/') for l in f)}
        except Exception as e:
            logger.critical("File errors can't be read. %s" % e)

        self.errorsPerActions = {}

        self.mutex = threading.Lock()

    def get_error_meaning(self, code):
        """Return error meaning according to its code
        """
        return self.errorsDict.get(str(code), "Unknown server error")

    def add_error(self, action, code, file):
        """ organize error per action. Save the file path per Code
        """
        self.mutex.acquire()

        if action not in self.errorsPerActions.keys():
            # create a new action key
            self.errorsPerActions[action] = {}

        if code not in self.errorsPerActions[action].keys():
            # create a new code key
            self.errorsPerActions[action][code] = []

        # add image path in the list
        self.errorsPerActions[action][code].append(file)

        # log in  an unique file an error per line
        if self.logToFile:
            out_base_path = self.create_logs_folder()

            file_path = os.path.join(out_base_path, 'ltu_errors.log')

            err = str(datetime.now()) + " " + action + " " + str(code)
            if code:
                err += " " + self.get_error_meaning(code) + " " + file + "\n"
            else:
                err += " " + self.get_error_meaning(code) + " " + "None-file" + "\n"

            try:
                with open(file_path, 'a') as file:
                    file.writelines(err)
            except Exception as e:
                    logger.critical('Could not create the log file: {}'.format(e))

            finally:
                self.mutex.release()

    def create_logs_folder(self):
        # create a folder to save the logs
        out_base_path = os.path.join(os.getcwd(), ".logs")
        if not os.path.exists(out_base_path):
            try:
                os.mkdir(out_base_path)
            except Exception as e:
                logger.critical('Could not create the folder ".logs" : {}'.format(e))
                sys.exit(-1)

        return out_base_path

    def print_errors(self):
       """ print all the errors"""
       data = self.errorsPerActions
       for action, codes in data.items():
           print("Action {}:".format(action))
           for code, img_list in codes.items():
               print("  Errors code {}:".format(code))
               for img in img_list:
                   print("     * Images file: {}".format(img))

    def get_actions(self):
        """return list of actions"""
        return self.errorsPerActions.keys()

    def get_code_per_action(self, action):
        """return error code per action
        """
        return self.errorsPerActions[action].keys()

    def get_image_per_code(self, action, code):
        """return image failed per code
        """
        return self.errorsPerActions[action][code]

    def log_in_file(self, list_txt):
        # log formated text in a file
        out_base_path = self.create_logs_folder()

        file_name = datetime.now().strftime('%Y-%m-%d_%H-%M-%S') + "_ltu_errors.log"
        file_path = os.path.join(out_base_path, file_name)

        try:
            with open(file_path, 'w') as file:
                file.write("\n".join(list_txt))
        except Exception as e:
                logger.critical('Could not create the log file: {}'.format(e))
