import pytest
import os
import json
import mock
import logging
import unittest
import uuid

from ltu.engine.result import Result
from ltu.engine.client import BaseClient, QueryClient, ModifyClient

logger = logging.getLogger()


def setConfig():
    app_key = os.environ.get('CI_API_KEY', None)
    server_url = os.environ.get('CI_SERVER_URL', None)

    if app_key is not None and server_url is not None:
        return (app_key, server_url)

    for root, dirs, files in os.walk(os.getcwd()):
        for file in files:
            if file == "config.json":
                try:
                    logger.info('Loading local configuration file')
                    with open(os.path.join(root, file), 'r') as f:
                        json_config = json.load(f)
                        logger.info('Configuration: {}'.format(json_config))
                        return (json_config["application_key"], json_config["server_url"])
                except IOError:
                    logger.error('Could not load or read configuration file')

    return (None, None)


# GLOBAL
(APPKEY, URL_SERVER) = setConfig()
config_only = pytest.mark.skipif(APPKEY is None,
                                  reason="config.json file required")


class TestClient(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        # Mocked answers
        self.search_image_result = """{"images": [{"keywords": [], "score": 0.10410962999999999, "id": "4ea5cc294f13c137cc000063", "result_info": "{\\n  \\"category\\" : \\"LOCALMATCHING\\",\\n  \\"query\\" : \\n  {\\n    \\"originalDimensions\\" : [500, 718],\\n    \\"resizedDimensions\\" : [356, 512],\\n    \\"matchingBox\\" : \\n    {\\n      \\"topLeftPoint\\" : [0.0197, 0.0449],\\n      \\"bottomRightPoint\\" : [0.9663, 0.9922]\\n    }\\n  },\\n  \\"reference\\" : \\n  {\\n    \\"originalDimensions\\" : [1000, 1435],\\n    \\"resizedDimensions\\" : [356, 512],\\n    \\"matchingBox\\" : \\n    {\\n      \\"topLeftPoint\\" : [0.0197, 0.0449],\\n      \\"bottomRightPoint\\" : [0.9663, 0.9922]\\n    }\\n  },\\n  \\"homography\\" : \\n  {\\n    \\"source\\" : \\"reference\\",\\n    \\"destination\\" : \\"query\\",\\n    \\"coefficients\\" : [1.0005, 0.0005, -0.0004, -0.0003, 1.0009, -0.0001, -0.0000, 0.0008, 1.0000]\\n  }\\n}"}, {"keywords": [], "score": 0.18547931300000001, "id": "4ea60983a34d4b41fd000065", "result_info": "{\\n  \\"category\\" : \\"LOCALMATCHING\\",\\n  \\"query\\" : \\n  {\\n    \\"originalDimensions\\" : [500, 718],\\n    \\"resizedDimensions\\" : [356, 512],\\n    \\"matchingBox\\" : \\n    {\\n      \\"topLeftPoint\\" : [0.0197, 0.0449],\\n      \\"bottomRightPoint\\" : [0.9663, 0.9238]\\n    }\\n  },\\n  \\"reference\\" : \\n  {\\n    \\"originalDimensions\\" : [1000, 1435],\\n    \\"resizedDimensions\\" : [356, 512],\\n    \\"matchingBox\\" : \\n    {\\n      \\"topLeftPoint\\" : [0.0169, 0.0449],\\n      \\"bottomRightPoint\\" : [0.9663, 0.9238]\\n    }\\n  },\\n  \\"homography\\" : \\n  {\\n    \\"source\\" : \\"reference\\",\\n    \\"destination\\" : \\"query\\",\\n    \\"coefficients\\" : [0.9990, 0.0005, -0.0003, -0.0002, 0.9990, 0.0006, 0.0000, 0.0000, 1.0000]\\n  }\\n}"}], "status": {"message": "No error", "code": 0}, "nb_results_found": 2}"""  # noqa: E501, E261
        self.get_application_status_result = """{"status": {"message": "No error", "code": 0}, "nb_loaded_images": 32995}"""  # noqa: E501, E261
        self.add_image_result = """{"status": {"message": "Image added to the reference database", "code": 0}, "task_id": 0}"""  # noqa: E501, E261
        self.add_image_error_result = """{"status": {"message": "status='-1404' status_message='Invalid chars in keyword'", "code": -1404}, "task_id": 0}"""  # noqa: E501, E261
        self.delete_image_result = """{"status": {"message": "Image deleted from the reference database", "code": 0}, "task_id": 0}"""  # noqa: E501, E261
        self.get_application_status_result = """{  "status": {"message": "No error", "code": 0}, "nb_loaded_images": 1}"""  # noqa: E501, E261
        self.get_image_not_exists_result = """{ "status": { "message":"Unknown image id", "code":-2601}, "image":null}"""  # noqa: E501, E261
        self.get_image_exists_result = """{ "status": { "message":"No error", "code":0}, "image": {"keywords":[], "score":0, "id":"ID", "result_info":""}}"""  # noqa: E501, E261

        self.longMessage = True
        self.application_key = APPKEY
        self.server_url = URL_SERVER
        if not APPKEY:
            # Mock base function that connect to Engine when creating clients.
            BaseClient.get_application_status = \
                mock.MagicMock(return_value=self.get_application_status_result)
            BaseClient._connect = mock.MagicMock(return_value=True)
        self.query_client = QueryClient(self.application_key, self.server_url)
        self.modify_client = ModifyClient(self.application_key, self.server_url)
        self.image_path = os.path.join(os.path.abspath(os.path.dirname(__file__)),
                                            "../../data/the-stacks-of-books.jpg")

    @config_only
    def testInit(self):
        client = BaseClient(self.application_key, self.server_url)
        self.assertEqual(self.application_key, client.application_key)
        self.assertEqual(self.server_url, client.server_url)
        self.assertEqual(self.query_client.application_key, client.application_key)
        self.assertEqual(self.modify_client.application_key, client.application_key)

    def testSearchImageByUploadMock(self):
        self.query_client.open_service = mock.MagicMock(return_value=self.search_image_result)
        result = self.query_client.search_image(self.image_path)

        self.assertEqual(2, len(result.images))
        self.assertEqual(2, result.nb_results_found)
        self.assertIsNotNone(result.images[0].result_info)
        self.assertEqual("LOCALMATCHING", result.images[0].result_info["category"])
        self.assertEqual([356, 512], result.images[0].result_info["query"]["resizedDimensions"])
        self.assertEqual("No error", result.status_message)

    @config_only
    def testSearchImageByUpload(self):
        # search image using the image path
        result = self.query_client.search_image(self.image_path)
        self.assertEqual("No error", result.status_message)

        # search image using its file content
        with open(self.image_path, "rb") as query_file:
            result = self.query_client.search_image_buffer(query_file)
            self.assertEqual("No error", result.status_message)

    @config_only
    def testSearchImageById(self):
        # search image using the image ID
        result = self.query_client.search_image_by_id("best-cities-to-see-street-art-2-2.jpg")
        self.assertEqual("No error", result.status_message)

    @config_only
    def testGetApplicationStatus(self):
        result = self.query_client.get_application_status()
        self.assertEqual(0, result.status_code)

    def testGetApplicationStatusMock(self):
        self.query_client.open_service = mock.MagicMock(
                                return_value=self.get_application_status_result)
        result = self.query_client.get_application_status()
        self.assertEqual(0, result.status_code)
        self.assertEqual(1, result.nb_loaded_images)

    @config_only
    def testAddDeleteImage(self):
        image_id = uuid.uuid4()
        # Add simple image: Success
        result = self.modify_client.add_image(self.image_path, image_id)
        self.assertEqual(0, result.status_code)
        result = self.modify_client.delete_image(image_id)
        self.assertEqual(0, result.status_code)

        # Add image with one keyword: Success
        result = self.modify_client.add_image(self.image_path, image_id, keywords=["toto"])
        self.assertEqual(0, result.status_code)
        result = self.modify_client.delete_image(image_id)
        self.assertEqual(0, result.status_code)

        # Add image with muiltiple keywords: Success
        # add
        keywords = ["toto", "tata"]
        result = self.modify_client.add_image(self.image_path, image_id, keywords=keywords)
        self.assertEqual(0, result.status_code)
        # get an test keywords
        result = self.modify_client.get_image(image_id)
        self.assertEqual(sorted(keywords), sorted(result.image.keywords))

        # del
        result = self.modify_client.delete_image(image_id)
        self.assertEqual(0, result.status_code)

        #  Add image with wrong keywords format: Error
        self.modify_client.open_service = mock.MagicMock(return_value=self.add_image_error_result)
        result = self.modify_client.add_image(self.image_path, "myimage", keywords=";*()[]")
        self.assertEqual(-1404, result.status_code)
        self.assertEqual("status='-1404' status_message='Invalid chars in keyword'",
                         result.status_message)

    def testAddImageMock(self):
        BaseClient.get_image = mock.MagicMock(return_value=Result(self.get_image_not_exists_result))
        # Success
        self.modify_client.open_service = mock.MagicMock(return_value=self.add_image_result)
        result = self.modify_client.add_image(self.image_path, "myimage")
        self.assertEqual(0, result.status_code)

        # Error
        self.modify_client.open_service = mock.MagicMock(return_value=self.add_image_error_result)
        result = self.modify_client.add_image(self.image_path, "myimage", keywords=";*()[]")
        self.assertEqual(-1404, result.status_code)
        self.assertEqual("status='-1404' status_message='Invalid chars in keyword'",
                         result.status_message)

    @config_only
    def testDeleteImage(self):
        self.modify_client.open_service = mock.MagicMock(return_value=self.delete_image_result)
        result = self.modify_client.delete_image("myimage")
        self.assertEqual(0, result.status_code)
        self.assertEqual("Image deleted from the reference database", result.status_message)

    @config_only
    def testCheck(self):
        self.query_client.get_application_status = mock.MagicMock(
          return_value=Result(self.get_application_status_result))
        self.assertTrue(self.query_client.check_status())
