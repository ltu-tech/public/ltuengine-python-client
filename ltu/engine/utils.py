import errno
import logging
import os

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def mkdir_recursive(path):
    """
    create path directory
    """
    try:
        sub_path = os.path.dirname(path)
        if not os.path.exists(sub_path):
            mkdir_recursive(sub_path)
        if not os.path.exists(path):
            os.mkdir(path)
    except OSError as e:
        # avoid thread conflict (loss of performance with mutex)
        if e.error == errno.EEXIST:  # path exists error
            path
        else:
            raise OSError("Can't create the directory %s ! %s" % (path, e))
    except Exception as e:
        logger.critical("Can't create the directory %s ! %s" % (path, e))
        raise
