#!/usr/bin/env python

from admin import ltuengine_admin
import logging
import begin

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


@begin.start
def ltuengine_admin_v3(
    actions: "A list of actions to execute on an application: "
             "download image and this thumbnail or download all the content : "
             "dl_img, dl_all_img, dl_img_thumbnail, dl_all_img_thumbnail",
    application_key: "LTU Engine application private key",
    db_user: "LTU Engine application database user",
    db_pwd: "LTU Engine application database password",
    image_id: "image name"=None,
    out_path: "folder path where save image to dowload"='./out/',
    size: "thumbnail height"=0,
    host: "server URL that hosts the application, default is LTU OnDemand"=None,
    dbsi_host: "server URL that hosts the application configuration databases"=None,
    dbapp_host: "server URL that hosts the application content databases, "
    "default behavior is to use dbsi_host value"=None,
    db_port: "Postgres Database port"=7791,
    nb_threads: "a list(separate each action by a comma) "
                "of number of threads"="1"):
    """
    see fonction ltuengine_admin in admin.py
    """

    return ltuengine_admin(actions,
                           application_key,
                           db_user,
                           db_pwd,
                           image_id,
                           out_path,
                           size,
                           host,
                           dbsi_host,
                           dbapp_host,
                           db_port,
                           nb_threads)
