#!/usr/bin/env python
import begin
import coloredlogs
import logging
from multiprocessing.dummy import Pool
from tqdm import tqdm

from application import Application

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def get_thumb_closest_resolution(size):
    """
    find closest thumbnails resolution to size parameter
    """

    thumbnail_size = [(32, 32), (64, 64), (128, 128), (150, 100), (200, 200), (512, 512)]
    for resolution in thumbnail_size:
        height = resolution[0]
        width = resolution[1]
        if size < resolution[0]:
             if ((size - height) < (int(resolution[0]) - size)):
                 return height, width
             else:
                 return resolution[0], resolution[1]

    return height, width


def downloadAllImages(application, out_path, dl_thumbnail, size, nb_threads=1):
    """
    download all images of the given application and their thumbnails if dl_thumbnail is set to true
    use size parameter to specify thumbnails size
    """

    images = application.findApplicationImage()
    images_keywords = application.findApplicationImageKeywords()

    label = " images with thumbnails " if dl_thumbnail == 1 else " images "

    # Download threading
    pool = Pool(processes=nb_threads)
    items = [(image[1], out_path, dl_thumbnail, size, images_keywords.get(image[1], []))
             for image in images]
    pool_iterable = pool.imap_unordered(application.downloadThumbnailImageForThread, items)
    progress_bar_items = tqdm(total=len(items),
                              iterable=pool_iterable,
                              unit=label,
                              desc='{0: <30}'.format("Download"))

    for item in progress_bar_items:
        pass

    # Download verification
    images_after = application.findApplicationImage()
    checkDownloadIntegrity(images, images_after)


def checkDownloadIntegrity(images_before_dl, images_after_dl):
    """
    Compare parameter lists and return differencies
    """
    # elements in images_after_dl but not in images_before_dl
    images_added = set(images_after_dl).difference(images_before_dl)
    # elements in images_before_dl but not in images_after_dl
    images_deleted = set(images_before_dl).difference(images_after_dl)

    differencies = set(images_added).union(images_deleted)
    if len(differencies) == 0:
        logger.info("All database images succefully downloaded")
        return

    if len(images_added) != 0:
        for img in images_added:
            logger.info("%s was added", img[1])

    if len(images_deleted) != 0:
        for img in images_deleted:
            logger.info("%s was deleted", img[1])


@begin.start
def ltuengine_admin(actions,
                    application_key,
                    db_user,
                    db_pwd,
                    image_id=None,
                    out_path='./out/',
                    size=0,
                    host=None,
                    dbsi_host=None,
                    dbapp_host=None,
                    db_port=7791,
                    nb_threads=1):
    """
    For an application, allow to :
     - get the image or the thumbnail URL
     - download an image or its thumbnails

    """

    coloredlogs.install(level='info')

    application = Application(host, application_key,
                              dbsi_host, dbapp_host,
                              db_user, db_pwd, db_port)

    if size != 0:
        size = get_thumb_closest_resolution(int(size))
        logger.info("The thumbnail size is %s", size)

    dl_thumbnail = ("_thumbnail" in actions)

    if "dl_img" in actions:
        if image_id:
            application.downloadThumbnailImage(image_id, out_path, dl_thumbnail, size)
            logger.info("Image {} succefully downloaded in directory {}".format(image_id, out_path))
        else:
            raise IOError("image_id unknown. Can't download it.")

    if "dl_all_img" in actions:
        downloadAllImages(application, out_path, dl_thumbnail, size, int(nb_threads))
